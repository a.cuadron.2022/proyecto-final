
"""change_colors producirá una nueva imagen con los colores cambiados"""
def change_colors(image: list[list[tuple[int, int, int]]],
                  to_change: tuple[int, int, int],
                  to_change_to: tuple[int, int, int]) -> list[list[tuple[int, int, int]]]:

    new_image = []
    for row in image:
        new_row = []
        for pixel in row:
            if pixel == to_change:
                new_pixel = to_change_to
            else:
                new_pixel = pixel
            new_row.append(new_pixel)
        new_image.append(new_row)
    return new_image


"""rotate_right producirá una nueva imagen girada 90 grados hacia la derecha"""
def rotate_right(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:

    rotated = []
    for x in range(len(image[0])):
        rotated.append([])
        for y in range(len(image) - 1, -1, -1):
            rotated[x].append(image[y][x])
    return rotated


"""mirror producirá una nueva imagen "espejada" según un eje vertical situado en la mitad de la imagen"""
def mirror(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:

    mirrored = []
    width = len(image[0])
    for row in image:
        new_row = []
        for i, pixel in enumerate(row):
            mirror_idx = width - i - 1
            new_row.append(row[mirror_idx])
        mirrored.append(new_row)
    return mirrored


"""rotate_colors producirá una nueva imagen con los colores cambiados. Para cambiarlos, sumará
a cada uno de los componentes RGB el número que se le indique como incremento, teniendo en cuenta
que si el valor llega a 255, tendrá que volver a empezar en 0"""
def rotate_colors(image: list[list[tuple[int, int, int]]], increment: int) -> list[list[tuple[int, int, int]]]:

    rotated = []
    for row in image:
        new_row = []
        for pixel in row:
            new_pixel = (pixel[0] + increment) % 256, (pixel[1] + increment) % 256, (pixel[2] + increment) % 256
            new_row.append(new_pixel)
        rotated.append(new_row)
    return rotated


"""blur creará una nueva imagen donde el valor RGB de cada pixel será el valor medio de los valores RGB
de los pixels que tiene encima, debajo, a la derecha y a la izquierda """
def blur(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:

    blurred = []
    for y in range(len(image)):
        new_row = []
        for x in range(len(image[0])):
            r_sum = 0
            g_sum = 0
            b_sum = 0
            count = 0
            if y > 0:
                r_sum += image[y - 1][x][0]
                g_sum += image[y - 1][x][1]
                b_sum += image[y - 1][x][2]
                count += 1
            if y < len(image) - 1:
                r_sum += image[y + 1][x][0]
                g_sum += image[y + 1][x][1]
                b_sum += image[y + 1][x][2]
                count += 1
            if x > 0:
                r_sum += image[y][x - 1][0]
                g_sum += image[y][x - 1][1]
                b_sum += image[y][x - 1][2]
                count += 1
            if x < len(image[0]) - 1:
                r_sum += image[y][x + 1][0]
                g_sum += image[y][x + 1][1]
                b_sum += image[y][x + 1][2]
                count += 1
            r_avg = r_sum // count
            g_avg = g_sum // count
            b_avg = b_sum // count
            new_row.append((r_avg, g_avg, b_avg))
        blurred.append(new_row)
    return blurred


"""shift desplazará la imagen el número de pixels que se indique, en el eje horizontal o vertical, según
el parámetro que se indique"""
def shift(image: list[list[tuple[int, int, int]]], horizontal: int, vertical: int) -> list[list[tuple[int, int, int]]]:

    shifted = []
    for y in range(len(image)):
        new_y = y + vertical
        if 0 <= new_y < len(image):
            new_row = []
            for x in range(len(image[0])):
                new_x = x + horizontal
                if 0 <= new_x < len(image[0]):
                    new_row.append(image[new_y][new_x])
            shifted.append(new_row)
    return shifted


"""crop creará una nueva imagen que contendrá solo los pixels que se encuentren dentro de un rectángulo que se
especifique"""
def crop(image: list[list[tuple[int, int, int]]], x: int, y: int, width: int, height: int) -> list[list[tuple[int, int, int]]]:

    cropped = []
    for i in range(height):
        cropped.append([])
        for j in range(width):
            cropped[i].append(image[y + i][x + j])
    return cropped


"""grayscale creará una nueva imagen que contendrá la imagen original, pero en escala de grises"""
"""CORRECTO"""
import transforms
def grayscale(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:

    grayscaled = []
    for row in image:
        new_row = []
        for pixel in row:
            r, g, b = pixel
            avg = (r + g + b) // 3
            gray = (avg, avg, avg)
            new_row.append(gray)
        grayscaled.append(new_row)
    return grayscaled


"""filter creará una nueva imagen que contendrá la imagen original, pero con un filtro aplicado a todos sus
pixels"""
import transforms

def filter(image: list[list[tuple[int, int, int]]], r: float, g: float, b: float) -> list[list[tuple[int, int, int]]]:

  filtered = []
  for row in image:
    new_row = []
    for pixel in row:
      r_val, g_val, b_val = pixel
      new_r = min(int(r * r_val), 255)
      new_g = min(int(g * g_val), 255)
      new_b = min(int(b * b_val), 255)
      new_pixel = (new_r, new_g, new_b)
      new_row.append(new_pixel)
    filtered.append(new_row)
  return filtered


