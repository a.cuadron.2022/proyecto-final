

"""Éste programa funcionará como el anterior, pero admitiendo varios nombres
 de función transformadora, si es caso seguidos por sus parámetros."""

from transforms import rotate_colors, change_colors, shift, mirror, blur
import sys
from PIL import Image

def transform_image(img, transform_name, *args):
    if transform_name == 'rotate_colors':
        img = rotate_colors(img, args[0])
    elif transform_name == 'change_colors':
        args_one = tuple(args[:3])
        args_two = tuple(args[3:])
        new_img = change_colors(img, args_one, args_two)
    elif transform_name == 'shift':
        img = shift(img, args[0], args[1])
    elif transform_name == 'mirror':
        img = mirror(img)
    elif transform_name == 'blur':
        img = blur(img)
    return img
def main():
    input_file = sys.argv[1]
    transform_names = sys.argv[2:]
    img = Image.open(input_file)
    for i in range(0, len(transform_names), 2):
        name = transform_names[i]
        args = transform_names[i + 1:] if len(transform_names) > i + 1 else []
        img = transform_image(img, name, *args)
    base_name = input_file.split('.')[0]
    output_file = f"{base_name}_trans.{input_file.split('.')[1]}"
    img.save(output_file)

if __name__ == '__main__':
    main()
