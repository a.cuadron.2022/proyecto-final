

"""Éste programa aceptará dos parámetros: el nombre de un fichero de imagen en formato RGB,
y el nombre de una función de transformación entre las siguients: rotate_right, mirror, blur,
greyscale"""
from images import read_img, write_img
from transforms import rotate_right, mirror, blur, grayscale
import sys
from PIL import Image
def main():

  if len(sys.argv) != 3:
    print("Uso: python transform_simple.py imagen transformacion")
    sys.exit(1)
  filename = sys.argv[1]
  transform = sys.argv[2]
  image = Image.open(filename)
  pixels = read_img(filename)
  if transform == "rotate_right":
    transformed = rotate_right(pixels)
  elif transform == "mirror":
    transformed = mirror(pixels)
  elif transform == "blur":
    transformed = blur(pixels)
  elif transform == "greyscale":
    transformed = grayscale(pixels)
  else:
    print("Transformación no válida")
    sys.exit(1)
  new_filename = filename.split(".")[0] + "_trans." + filename.split(".")[1]
  transformed_image = Image.new(image.mode, image.size)
  write_img(transformed, new_filename)

if __name__ == '__main__':
  main()


