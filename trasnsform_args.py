

"""Éste programa aceptará dos o más parámetros. El primer parámetro será el nombre
 de un fichero de imagen en formato RGB. El segundo parámetro será el nombre de una
 función de transformación cualquiera entre las que se hayan implementado"""
from transforms import change_colors, rotate_colors, shift
import sys
from PIL import Image
def main():

    input_file = sys.argv[1]
    transform_name = sys.argv[2]
    args = sys.argv[3:]
    img = Image.open(input_file)
    if transform_name == "change_colors":
        args_one=tuple(args[:3])
        args_two=tuple(args[3:])
        new_img = change_colors(img, args_one, args_two)
    elif transform_name == "rotate_colors":
        new_img = rotate_colors(img, args[0])
    elif transform_name == "shift":
        new_img = shift(img, args[0], args[1])
    base_name = input_file.split('.')[0]
    output_file = f"{base_name}_trans.{input_file.split('.')[1]}"
    new_img.save(output_file)

if __name__ == '__main__':
    main()

